public class Fridge
{
    private String color;
    private int voltage;
    private int weight;

    public Fridge(String color, int voltage, int weight)
    {
        this.color = color;
        this.voltage = voltage;
        this.weight = weight;
    }

    public String getColor() 
    {
        return this.color;
    }

    public void setColor(String newColor) 
    {
        color = newColor;
    }

    public int getVoltage() 
    {
        return this.voltage;
    }

    public void setVoltage(int newVoltage) 
    {
        voltage = newVoltage;
    }

    public int getWeight() 
    {
        return this.weight;
    }

    public void setWeight(int newWeight) 
    {
        weight = newWeight;
    }

    public void printColor (String color)
    {
        System.out.println("The fridge color is " + color);
    }

    public void printWeight (int weight)
    {
        System.out.println("The weight of the fridge is " + weight);
    }

    public void newColors(String newColor)
    {

        if(color.equals("White"))
        {
            color = newColor;
            System.out.println(color);
        }

        else if(color.equals("Black"))
        {
            color = newColor;
            System.out.println(color);
        }

        else{
            color = color + " glossy";
            System.out.println(color);
        }
    }
}