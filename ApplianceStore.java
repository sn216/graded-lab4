import java.util.Scanner;
public class ApplianceStore
{
    public static void main (String args[])
    {
        Scanner scan = new Scanner(System.in);
        Fridge[] appliance = new Fridge [4];
        
        for(int i = 0; i < 4; i++)
        {
            System.out.println("Enter the fridge color");
            String userColor = scan.next();

            System.out.println("Enter the fridge voltage");
            int userVoltage = scan.nextInt();

            System.out.println("Enter the fridge weight");
            int userWeight = scan.nextInt();

            appliance[i] = new Fridge(userColor, userVoltage, userWeight);
        }

        System.out.println("Enter a new color");
        String newColor = scan.next();
        appliance[3].setColor(newColor);

        System.out.println("Enter a new voltage");
        int newVoltage = scan.nextInt();
        appliance[3].setVoltage(newVoltage);

        System.out.println("Enter a new weight");
        int newWeight = scan.nextInt();
        appliance[3].setWeight(newWeight);

        System.out.println("Color: " + appliance[3].getColor() + " Voltage:" + appliance[3].getVoltage() + " Weight: " + appliance[3].getWeight() );

       // appliance[0].printColor(appliance[0].color);
       // appliance[0].printWeight(appliance[0].weight);

        appliance[1].newColors("Red");
       
        scan.close();
    }
}
